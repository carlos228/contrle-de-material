package com.example.eden;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EditText editNomeMaterial;
    private EditText editDataSaida;
    private EditText editNomeAutorizador;
    private EditText editNomeSolicitante;
    private List<Material> materialList;
    private int id;
    private int idMaximo = 0;
    private int idUtimoOriginal = 0;
    private boolean p = true;
    private String dataEmprestimo;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editNomeMaterial = findViewById(R.id.editMaterial);
        editNomeAutorizador = findViewById(R.id.editAutorizado);
        editNomeSolicitante = findViewById(R.id.editEntregue);


        materialList = new ArrayList<Material>();
        DatabaseReference reference = FirebaseFunctions.getReference();
        reference = reference.child("Saida");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dados : dataSnapshot.getChildren()) {
                    Material material = dados.getValue(Material.class);
                    materialList.add(material);
                    idUtimoOriginal = materialList.get(materialList.size()-1).getIdEmprestimo();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });

        DateFormat formatoData = new SimpleDateFormat("dd-MM-yyyy");
        Date data = new Date();
        dataEmprestimo = formatoData.format(data);


    }

    public void saida (View v){
        String nomeMaterial = editNomeMaterial.getText().toString();
        String nomeSolicitante = editNomeSolicitante.getText().toString();
        String nomeAutorizador = editNomeAutorizador.getText().toString();

        if(verificaEspacosVazios(nomeMaterial,nomeAutorizador,nomeSolicitante)){
            if(p){
                p = false;
                id = idUtimoOriginal;
            }
            id++;
            Material material = new Material(
                    nomeMaterial,
                    dataEmprestimo,
                    nomeSolicitante,
                    nomeAutorizador
            );



            DatabaseReference reference = FirebaseFunctions.getReference().child("Saida");
            DatabaseReference fora = FirebaseFunctions.getReference().child("Fora");
            material.setIdEmprestimo(id);

            reference.child(material.getIdEmprestimo()+"").setValue(material);
            fora.child(material.getIdEmprestimo()+"").setValue(material).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Toast.makeText(getApplicationContext(),"Material Emprestado com sucesso",Toast.LENGTH_SHORT).show();
                }
            });
        }else{
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(v.getContext())
                    .setTitle("Aviso")
                    .setMessage("Preencha todos os campos")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

            alertDialog.create();
            alertDialog.show();

        }



    }

    public void calculaId() {

    }


    public void abrirTelaListaEmprestados(View v){
        Intent intent = new Intent(this,ListaEmprestadosActivity.class);
        startActivity(intent);
        finish();
    }

    private boolean verificaEspacosVazios(String nomeMaterial,String nomeRequerente,String nomeAutorizador){
        boolean permicao = true;
        if (nomeAutorizador.equals("") || nomeRequerente.equals("") || nomeMaterial.equals("")){
            permicao = false;
        }
        return permicao;
    }
}
