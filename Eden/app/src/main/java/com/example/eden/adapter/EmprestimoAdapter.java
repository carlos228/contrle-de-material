package com.example.eden.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eden.FirebaseFunctions;
import com.example.eden.ListaEmprestadosActivity;
import com.example.eden.Material;
import com.example.eden.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.List;

public class EmprestimoAdapter extends RecyclerView.Adapter<EmprestimoAdapter.MyViewHolder> {

    private List<Material> emprestimos;
    private ListaEmprestadosActivity context;

    public EmprestimoAdapter(List<Material> materiais,ListaEmprestadosActivity context){
        this.emprestimos = materiais;
        this.context = context;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLista = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lista,parent,false);
        return new MyViewHolder(itemLista);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        Material material = emprestimos.get(position);
        holder.nome.setText("Material: "+material.getNomeMaterial());
        holder.requerente.setText("Requerente: "+material.getNomeMembroEmprestimo());
        holder.autorizador.setText("Autorizado por: "+material.getNomeMembroAtorizacao());
        holder.data.setText("Data de saida: "+material.getDataEmprestimo());
        final int id = material.getIdEmprestimo();
        holder.devolvido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatabaseReference dataRetorno;
                DatabaseReference retornou;

                holder.foraRef = FirebaseFunctions.getReference().child("Fora");
                holder.saidaRef = FirebaseFunctions.getReference().child("Saida");
                holder.foraRef = holder.foraRef.child(""+id);
                dataRetorno = holder.saidaRef.child(""+id).child("dataRetorno");
                retornou = holder.saidaRef.child(""+id).child("retornou");
                dataRetorno.setValue(context.getDataEmprestimo());
                retornou.setValue(true);

                holder.foraRef.removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(context.getApplicationContext(),"Material devolvido",Toast.LENGTH_SHORT).show();
                    }
                });

                Intent intent = new Intent(context,ListaEmprestadosActivity.class);
                context.startActivity(intent);
                context.finish();

            }
        });



    }

    private List<Material> removeElemento(List<Material> list,int id){
        for(int i=0;i<list.size();i++){
            if(list.get(i).getIdEmprestimo() == id){
                list.remove(i);
            }
        }
        return list;
    }

    @Override
    public int getItemCount() {
        return emprestimos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView nome,data,autorizador,requerente;
        Button devolvido;
        DatabaseReference foraRef;
        DatabaseReference saidaRef;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            nome = itemView.findViewById(R.id.adapterNome);
            data = itemView.findViewById(R.id.adapterDataEmprestimo);
            autorizador = itemView.findViewById(R.id.adapterResponsavel);
            requerente = itemView.findViewById(R.id.adapterRequerente);
            devolvido = itemView.findViewById(R.id.btnDevolver);
        }
    }
}
