package com.example.eden;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.eden.adapter.EmprestimoAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.DataFormatException;

public class ListaEmprestadosActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private EmprestimoAdapter adapter;
    private List<Material> materialList;
    private DatabaseReference foraRef;
    private ValueEventListener carregar;
    private String dataEmprestimo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_emprestados);
        recyclerView = findViewById(R.id.recyclerViewListaEmprestimos);
        materialList = new ArrayList<Material>();

        //Configurações iniciais
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.hasFixedSize();
        adapter = new EmprestimoAdapter(materialList,this);
        recyclerView.setAdapter(adapter);
        foraRef = FirebaseFunctions.getReference().child("Fora");
        carregarEmprestimos();

        DateFormat formatoData = new SimpleDateFormat("dd-MM-yyyy");
        Date data = new Date();
        dataEmprestimo = formatoData.format(data);
    }

    public void abrirTelaEmprestimo(View v){
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void carregarEmprestimos(){
        carregar = foraRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dados: dataSnapshot.getChildren()){
                    materialList.add( dados.getValue(Material.class));
                }

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    public void setRecyclerView(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    public EmprestimoAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(EmprestimoAdapter adapter) {
        this.adapter = adapter;
    }

    public List<Material> getMaterialList() {
        return materialList;
    }

    public void setMaterialList(List<Material> materialList) {
        this.materialList = materialList;
    }

    public DatabaseReference getForaRef() {
        return foraRef;
    }

    public void setForaRef(DatabaseReference foraRef) {
        this.foraRef = foraRef;
    }

    public ValueEventListener getCarregar() {
        return carregar;
    }

    public void setCarregar(ValueEventListener carregar) {
        this.carregar = carregar;
    }

    public String getDataEmprestimo() {
        return dataEmprestimo;
    }

    public void setDataEmprestimo(String dataEmprestimo) {
        this.dataEmprestimo = dataEmprestimo;
    }
}
