package com.example.eden;

public class Material {
    private String nomeMaterial;
    private String dataEmprestimo;
    private String nomeMembroEmprestimo;
    private String nomeMembroAtorizacao;
    private String dataRetorno;
    private int idEmprestimo;
    private boolean retornou;

    public Material() {

    }

    public Material(String nomeMaterial, String dataEmprestimo, String nomeMembroEmprestimo, String nomeMembroAtorizacao) {
        this.nomeMaterial = nomeMaterial;
        this.dataEmprestimo = dataEmprestimo;
        this.nomeMembroEmprestimo = nomeMembroEmprestimo;
        this.nomeMembroAtorizacao = nomeMembroAtorizacao;
        this.dataRetorno = "não devolvido";
        this.retornou = false;
    }

    public String getNomeMaterial() {
        return nomeMaterial;
    }

    public void setNomeMaterial(String nomeMaterial) {
        this.nomeMaterial = nomeMaterial;
    }

    public String getDataEmprestimo() {
        return dataEmprestimo;
    }

    public void setDataEmprestimo(String dataEmprestimo) {
        this.dataEmprestimo = dataEmprestimo;
    }

    public String getNomeMembroEmprestimo() {
        return nomeMembroEmprestimo;
    }

    public void setNomeMembroEmprestimo(String nomeMembroEmprestimo) {
        this.nomeMembroEmprestimo = nomeMembroEmprestimo;
    }

    public String getNomeMembroAtorizacao() {
        return nomeMembroAtorizacao;
    }

    public void setNomeMembroAtorizacao(String nomeMembroAtorizacao) {
        this.nomeMembroAtorizacao = nomeMembroAtorizacao;
    }

    public String getDataRetorno() {
        return dataRetorno;
    }

    public void setDataRetorno(String dataRetorno) {
        this.dataRetorno = dataRetorno;
    }

    public boolean isRetornou() {
        return retornou;
    }

    public void setRetornou(boolean retornou) {
        this.retornou = retornou;
    }

    public int getIdEmprestimo() {
        return idEmprestimo;
    }

    public void setIdEmprestimo(int idEmprestimo) {
        this.idEmprestimo = idEmprestimo;
    }
}
