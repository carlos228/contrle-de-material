package com.example.eden;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FirebaseFunctions {
    public static DatabaseReference getReference(){
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        return  databaseReference;
    }
}
